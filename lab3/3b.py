from random import randint
from math import fabs, sqrt, pow


class Point:
	def __init__(self, x, y):
		if x != None and y != None:
			self.x = x
			self.y = y
		self.x = randint(0,9)
		self.y = randint(0,9)


class Distance(Point):
	def __init__(self, p1, p2):
		self.length = sqrt(pow(p1.x-p2.x,2) + pow(p1.y-p2.y,2))
		self.length = fabs(self.length)


class List_Point:
	def __init__(self, n):
		self.list = []
		self.n = n

		for i in range(n):
			self.list.append(Point(x=None, y=None))

	def min_distance(self):
		return Distance(self.list[0], self.list[1]).length

def qsort(list):
		less = []
		equal = []
		greater = []
		if len(list) > 1:
			pivot = list[0]
			for item in list:
				if Distance(item, Point(0,0)).length < Distance(pivot, Point(0,0)).length:
					less.append(item)
				elif Distance(item, Point(0,0)).length > Distance(pivot, Point(0,0)).length:
					greater.append(item)
				else:
					equal.append(item)
			return qsort(less) + equal + qsort(greater)
		else:
			return list


closet_pair = List_Point(5)
closet_pair.list = qsort(closet_pair.list)
print(closet_pair.min_distance())

for i in closet_pair.list:
	print(i.x, i.y)