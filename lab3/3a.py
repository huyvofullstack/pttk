from random import randint
from math import fabs, sqrt, pow


class Point:
	def __init__(self):
		self.x = randint(0,9)
		self.y = randint(0,9)


class Distance(Point):
	def __init__(self, p1, p2):
		self.length = sqrt(pow(p1.x-p2.x,2) + pow(p1.y-p2.y,2))
		self.length = fabs(self.length)


class List_Point:
	def __init__(self, n):
		self.list = []
		self.n = n

		for i in range(n):
			self.list.append(Point())

	def min_distance(self):
		min = Distance(self.list[0], self.list[1]).length
		for i in self.list:
			for j in self.list:
				if i != j:
					if Distance(i,j).length < min:
						min = Distance(i,j).length
		return min


closet_pair = List_Point(5)
print(closet_pair.min_distance())

""" 
===============================================
Algorithm min_distance(list):
	min = distance(list[0],list[1])
	for i = 0 to list.length do:
		for j = i+1 to list.length do:
			if min > distance(list[i],list[j]):
				min = distance(list[i],list[j])
	return min

Big-O = O(n^2)
===============================================
"""
