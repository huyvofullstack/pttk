def multiply(matrix_1, matrix_2, row):
    matrix_3 = [[0 for x in range(row)] for x in range(row)]
    for i in range(row):
        for j in range(row):
            for k in range(row):
                matrix_3[i][j] += matrix_1[i][k] * matrix_2[k][j]
    return matrix_3
    
def enter_values(matrix, row):
    for i in range(row):
        for j in range(row):
            matrix[i][j] = int(input("matrix[{}][{}] = ".format(i, j)))
    return matrix
    
def print_matrix(matrix, row):
    for i in range(row):
        for j in range(row):
            print("{} ".format(matrix[i][j]), end='')
        print("\n")

row = int(input("row: "))    
matrix_1 = [[0 for x in range(row)] for x in range(row)]
matrix_2 = [[0 for x in range(row)] for x in range(row)]

matrix_1 = enter_values(matrix_1, row)
matrix_2 = enter_values(matrix_2, row)

matrix_3 = multiply(matrix_1, matrix_2, row)

print_matrix(matrix_3, row)
