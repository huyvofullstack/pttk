from random import randint

list = []

# Input range of list
n = int(input("Range: "))

# random each item in list
for i in range(n):
	list.append(randint(1, 100))

print("\nBefore: {}\n".format(list))

def selection_sort(list):
	for i in range(len(list)):
		smallest = i
		for j in range(i+1, len(list)):
			if list[j] < list[smallest]:
				smallest = j
		list[smallest], list[i] = list[i], list[smallest]
	return list

def insertion_sort(list):
	for i in range(1, len(list)):
		j = i
		while j > 0 and list[j-1] > list[j]:
			list[j-1], list[j] = list[j], list[j-1]
			j -= 1
	return list

def bubble_sort(list):
	flag = True
	length = len(list)
	while flag:
		flag = False
		for i in range(1, length):
			if list[i-1] > list[i]:
				flag = True
				list[i-1], list[i] = list[i], list[i-1]
		length -= 1
	return list

def merge(left, right):
    result = []
    i, j = 0, 0
    while i < len(left) and j < len(right):
        if left[i] <= right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
    result += left[i:]
    result += right[j:]
    return result

def merge_sort(list):
    if len(list) < 2:
        return list
    mid = len(list) // 2
    left = merge_sort(list[:mid])
    right = merge_sort(list[mid:])
    return merge(left, right)

def quicksort(list):
	less = []
	equal = []
	greater = []
	if len(list) > 1:
		pivot = list[0]
		for item in list:
			if item < pivot:
				less.append(item)
			elif item == pivot:
				equal.append(item)
			elif item > pivot:
				greater.append(item)
		return quicksort(less) + equal + quicksort(greater)
	else:
		return list


print("Selection Sort -> Enter 1")
print("Insertion Sort -> Enter 2")
print("Bubble Sort -> Enter 3")
print("Merge Sort -> Enter 4")
print("Quick Sort -> Enter 5\n")

try:
	input = int(input("> "))
except ValueError:
	print("Please enter a number...")
finally:
	if input == 1:
		print(selection_sort(list))
	elif input == 2:
		print(insertion_sort(list))
	elif input == 3:
		print(bubble_sort(list))
	elif input == 4:
		print(merge_sort(list))
	elif input == 5:
		print(quicksort(list))
