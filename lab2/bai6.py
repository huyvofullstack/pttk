def binomial_coefficient_recursive(n, k):
	if k == 0 or k == n:
		return 1
	return binomial_coefficient_recursive(n-1, k-1) + binomial_coefficient_recursive(n-1, k)

def binomial_coefficient(n, k):
	result = 1
	for i in range(1, k+1):
		result = result * n / i
		n -= 1
	return int(result)


#result = binomial_coefficient_recursive(4, 3)
result = binomial_coefficient(4, 3)
print(result)
