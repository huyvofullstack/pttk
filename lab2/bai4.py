def fib_recursion(n):
	if n <= 1:
		return n
	return fib_recursion(n-1) + fib_recursion(n-2)

n = int(input("n = "))

def fib(n):
	a, b = 0, 1
	for i in range(n):
		a, b = b, a + b
	return a

print("Recursion: {}".format(fib_recursion(n)))
print("Non-recursion: {}".format(fib(n)))
