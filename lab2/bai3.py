def binary_search_recursive(list, key):
	if len(list) < 2:
		if list[0] == key:
			return True
		else:
			return False
	mid = len(list) // 2
	if list[mid] > key:
		return binary_search_recursive(list[:mid], key)	
	elif list[mid] < key:
		return binary_search_recursive(list[mid:], key)
	else:
		return True
		
def binary_search(list, key):
    for i in range(len(list)):
        if list[i] == key:
            return True
    return False

list = [1, 2, 3, 4, 5]
#result = binary_search_recursive(list, 5)
result = binary_search(list, 4)
print(result)
