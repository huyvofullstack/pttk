from random import randint

# Input the row/column of two square matrices
n = int(input("n: \n"))

matrix_1 = [[0 for x in range(n)] for x in range(n)]

matrix_2 = [[0 for x in range(n)] for x in range(n)]

matrix_3 = [[0 for x in range(n)] for x in range(n)]

# random each item in matrix_1 and matrix_2
# set all items in matrix_3 equal to 0
for i in range(n):
	for j in range(n):
		matrix_1[i][j] = randint(0, 9)
		matrix_2[i][j] = randint(0, 9)

# multiply matrix_1 and matrix_2
for i in range(n):
	for j in range(n):
		for k in range(n):
			matrix_3[i][j] += matrix_1[i][k] * matrix_2[k][j]

# print out matrix_1
print("Matrix_1:\n")
for i in range(n):
	for j in range(n):
		print("{} ".format(matrix_1[i][j]), end='')
	print("\n")

# print out matrix_2
print("Matrix_2:\n")
for i in range(n):
	for j in range(n):
		print("{} ".format(matrix_2[i][j]), end='')
	print("\n")

# print out matrix_3
print("Matrix_3:\n")
for i in range(n):
	for j in range(n):
		print("{} ".format(matrix_3[i][j]), end='')
	print("\n")
