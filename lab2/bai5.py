def print_move(height, from_tower, to_tower):
	print("Moving disk {} from {} to {}.".format(height, from_tower, to_tower))

def move_disk_recursive(height, from_tower, to_tower, with_tower):
	if height >= 1:
		move_disk_recursive(height-1, from_tower, with_tower, to_tower)
		print_move(height, from_tower, to_tower)
		move_disk_recursive(height-1, with_tower, to_tower, from_tower)


height = int(input("height: "))
#move_disk_recursive(height, 'A', 'B', 'C')
move_disk(height, 'A', 'B', 'C')
