import math
import random

BANK_OFFER = 1

def deterministic(offers, real_estate):
	accepted_price = math.sqrt(real_estate)
	for offer in offers:
		if offer >= accepted_price:
			return offer
	else:
		return BANK_OFFER

def randomized(offers, real_estate):
	L = math.floor(math.log2(real_estate))
	j = random.randint(1, L)

	accepted_price = real_estate/(2**j)
	for offer in offers:
		if offer >= accepted_price:
			return offer
	else:
		return BANK_OFFER

if __name__ == '__main__':
	offers = [100, 200, 250, 456, 800, 550, 500]
	real_estate = 1000
	best_offer = randomized(offers, real_estate)
	print(best_offer)
