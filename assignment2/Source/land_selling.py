class Offer:

	def __init__(self, start=None, finish=None, price=None):
		self.start = start
		self.finish = finish
		self.price = price

	def overlap_s(self, offer):
		if self.start < self.finish:
			if offer.start < self.start < offer.finish:
				return True
			elif offer.start > offer.finish > self.start:
				return True
			else:
				return False
		elif self.start > self.finish:
			if offer.start < self.start < offer.finish:
				return True
			elif self.start > offer.start > offer.finish:
				return True
			else:
				return False
		else:
			return False

	def overlap_f(self, offer):
		if self.start < self.finish and offer.start < offer.finish:
			if self.start == offer.start:
				return True
		elif self.start > self.finish and offer.start > offer.finish:
			if self.start == offer.start:
				return True
		else:
			return False

	def is_separate_with(self, offer):
		if self.overlap_s(offer) or offer.overlap_s(self):
			return False
		elif self.overlap_f(offer) or offer.overlap_f(self):
			return False
		else:
			return True

	def minus_to(self, delta):
		self.start -= delta
		if self.start < 0:
			self.start += 360
		self.finish -= delta    
		if self.finish <= 0:
			self.finish += 360


class Seller:
	
	def __init__(self, offers=None):
		self.offers = offers

	def get_overlap_time(self):
		result = [0 for offer in self.offers]
		for i in range(len(self.offers)):
			for j in range(len(self.offers)):
				if i != j and self.offers[i].overlap_s(self.offers[j]):
					result[i] += 1
		return result

	def least_overlap_offer(self, overlap_time):
		prime = overlap_time.index(min(overlap_time))
		return self.offers[prime]

	def reject_offers_overlap_with(self, prime_offer):
		self.offers = [
			offer for offer in self.offers if not prime_offer.overlap_s(offer)
		]

	def reload_offers_base_on(self, prime_offer):
		delta = prime_offer.start
		for offer in self.offers:
			offer.minus_to(delta)

	def sort_offers_by_finish(self):
		self.offers.sort(key=lambda offer: offer.finish, reverse=False)

	def accepted_offers(self):
		self.sort_offers_by_finish()
		result = [self.offers[0]]
		for offer in self.offers:
			if offer.start >= result[len(result)-1].finish:
				result.append(offer)
		return result

	def maximum_profit(self):
		result = [offer.price for offer in self.offers]
		for i in range(1, len(self.offers)):
			for j in range(i):
				if self.offers[i].is_separate_with(self.offers[j]):
					result[i] = max(self.offers[i].price + result[j], result[i])
		return max(result)

	def set_up(self):
		"""Choose prime offer, reject offers overlap with prime offer and then
		reload the point of each offer base on prime."""
		overlap_time = self.get_overlap_time()
		prime_offer = self.least_overlap_offer(overlap_time)
		self.reject_offers_overlap_with(prime_offer)
		self.reload_offers_base_on(prime_offer)

if __name__ == '__main__':
	offers = [
		Offer(60, 100, 4),
		Offer(10, 50, 2),
		Offer(70, 200, 3),
		Offer(20, 70, 1),
		Offer(140, 300, 2),
		Offer(30, 60, 3),
		Offer(80, 150, 2),
		Offer(200, 350, 5),
		Offer(250, 50, 3)
	]
	
	seller = Seller(offers)
	
	seller.sort_offers_by_finish()
	print(seller.maximum_profit())

	seller.set_up()
	print(len(seller.accepted_offers()))
