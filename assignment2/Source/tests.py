import unittest
from land_selling import Offer, Seller


class LandSellingTest(unittest.TestCase):

	def setUp(self):
		offers1 = [
			Offer(0, 60, 2),
			Offer(10, 20, 4),
			Offer(30, 50, 1),
			Offer(50, 90, 3),
			Offer(70, 100, 2),
			Offer(100, 200, 5),
			Offer(340, 20, 1)
		]
		offers2 = [
			Offer(60, 100, 4),
			Offer(10, 50, 2),
			Offer(70, 200, 3),
			Offer(20, 70, 1),
			Offer(140, 300, 2),
			Offer(30, 60, 3),
			Offer(80, 150, 2),
			Offer(200, 350, 5),
			Offer(250, 50, 3)
		]
		offers3 = [
			Offer(170, 10, 12),
			Offer(70, 20, 10),
			Offer(170, 20, 8),
			Offer(200, 30, 6),
			Offer(30, 110, 4),
			Offer(220, 120, 2)
		]
		self.offers_list = [offers1, offers2, offers3]
		self.greedy_answers = [4, 3, 2]
		self.dynamic_answers = [13, 12, 16]

	def test_greedy(self):
		for offers, ans in zip(self.offers_list, self.greedy_answers):
			seller = Seller(offers)
			seller.set_up()
			self.assertEqual(len(seller.accepted_offers()), ans)

	def test_dynamic(self):
		for offers, ans in zip(self.offers_list, self.dynamic_answers):
			seller = Seller(offers)
			seller.sort_offers_by_finish()
			self.assertEqual(seller.maximum_profit(), ans)
		
if __name__ == '__main__':
	unittest.main()
