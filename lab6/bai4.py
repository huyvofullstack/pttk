parent = dict()
rank = dict()

def make_set(vertice):
    parent[vertice] = vertice
    rank[vertice] = 0

def find(vertice):
    if parent[vertice] != vertice:
        parent[vertice] = find(parent[vertice])
    return parent[vertice]

def union(vertice1, vertice2):
    root1 = find(vertice1)
    root2 = find(vertice2)
    if root1 != root2:
        if rank[root1] > rank[root2]:
            parent[root2] = root1
        else:
            parent[root1] = root2
            if rank[root1] == rank[root2]:
                rank[root2] += 1

def kruskal(graph):
    for vertice in graph['vertices']:
        make_set(vertice)

    minimum_spanning_tree = set()
    edges = list(graph['edges'])
    edges.sort()
    for edge in edges:
        weight, vertice1, vertice2 = edge
        if find(vertice1) != find(vertice2):
            union(vertice1, vertice2)
            minimum_spanning_tree.add(edge)
    return minimum_spanning_tree


if __name__ == '__main__':
    graph = {'vertices': ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'],
            'edges': set([
                (4, 'a', 'b'),
                (8, 'a', 'h'),
                (11, 'b', 'h'),
                (7, 'h', 'i'),
                (2, 'i', 'c'),
                (8, 'b', 'c'),
                (6, 'i', 'g'),
                (1, 'h', 'g'),
                (4, 'c', 'f'),
                (2, 'g', 'f'),
                (7, 'c', 'd'),
                (14, 'd', 'f'),
                (9, 'd', 'e'),
                (10, 'f', 'e')
            ])
    }   

    minimum_spanning_tree = kruskal(graph)
    print("weight", "src", "dst")
    for edge in minimum_spanning_tree:
        print(edge[0], ' '*4, edge[1], ' '*1, edge[2])
