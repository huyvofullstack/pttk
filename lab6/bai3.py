from bai2 import open_file

time = 0 

class Vertex:
	def __init__(self):
		self.adj = []
		self.color = None
		self.finish = None
		self.parent = None
		self.d = None

def DFS_Visit(G, u):
	global time
	time = time + 1
	u.d = time
	u.color = 'GRAY'
	for v in G:
		if v in u.adj != 0 and v.color == 'WHITE':
			v.parent = u
			DFS_Visit(G, v)
	u.color = 'BLACK'
	time = time + 1
	u.finish = time

def DFS(G):
	for v in G:
		v.color = 'WHITE'
		v.parent = None
	for u in G:
		if u.color == 'WHITE':
			DFS_Visit(G, u)

if __name__ == '__main__':
	G = []
	open_file(G, 'input.txt')
	DFS(G)
	for vertex in G:
		print(vertex.color, vertex.d, vertex.finish, vertex.parent)
