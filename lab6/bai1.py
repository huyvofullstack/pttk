def get_max(graph, scheduled):
	max = 0
	max_index = 0
	for v in range(len(graph)):
		if scheduled[v] == False and sum(graph[v]) > max:
			max = sum(graph[v])
			max_index = v
	return max_index

def scheduling(graph):
	label = [0 for i in range(len(graph))]
	scheduled = [False for i in range(len(graph))]
	schedules = 1
	
	for i in range(len(graph)):
		v = get_max(graph, scheduled)
		scheduled[v] = True
		label[v] = schedules
		for u in range(len(graph)):
			if graph[v][u] != 0 and label[u] == label[v]:
				label[v] += 1
		schedules = label[v]
	
	return label

if __name__ == "__main__":
	graph = [
		[0, 0, 0, 0, 0, 0, 0, 0],
		[0, 0, 0, 1, 0, 0, 0, 0],
		[0, 0, 0, 1, 1, 1, 1, 0],
		[0, 1, 1, 0, 0, 0, 1, 1],
		[0, 0, 1, 0, 0, 0, 1, 1],
		[0, 0, 1, 0, 0, 0, 1, 1],
		[0, 0, 1, 1, 1, 1, 0, 1],
		[0, 0, 0, 1, 1, 1, 1, 0]
	]
	result = scheduling(graph)
	print(result)
