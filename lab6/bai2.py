import math
import queue


class Vertex:
	def __init__(self):
		self.adj = []
		self.color = None
		self.parent = None
		self.d = None

def BFS(G, s):
	# Initialize
	Q = queue.Queue()
	for u in G:
		if u != s:
			u.color = 'WHITE'
			u.d = math.inf
			u.parent = None

	s.color = 'GRAY'
	s.d = 0
	s.parent = None

	Q.put(s)
	while Q.empty() == False:
		u = Q.get()
		for v in G:
			if v in u.adj and v.color == 'WHITE':
				v.color = 'GRAY'
				v.d = u.d + 1
				v.parent = u
				Q.put(v)
		u.color = 'BLACK'

def open_file(G, filename):
	with open(filename) as file:
		lines = file.readlines()
		for i in range(len(lines)):
			G.append(Vertex())
		
		for line, v in zip(lines, G):
			adj = list(line)
			if line != lines[len(lines)-1]:
				del adj[len(adj)-1]
			for i in adj:
				v.adj.append(G[int(i)])

if __name__ == '__main__':
	G = []
	open_file(G, 'input.txt')
	BFS(G, G[0])
	for vertex in G:
		print(vertex.color, vertex.d, vertex.parent)
