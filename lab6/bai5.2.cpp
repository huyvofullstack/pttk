#include <iostream>

using namespace std;

#define MAX 100
#define INF 99999


class Graph {
public:
	int n;
	int a[MAX][MAX];
public:
	Graph() {
		this->n = 0;
	}
	void addEdge(int i, int j, int weight);
};

void Graph::addEdge(int i, int j, int weight) {
	this->a[i][j] = weight;
}

void printPathFloyd(Graph &g) {
	for (int i = 0; i < g.n; i++) {
		for (int j = 0; j < g.n; j++) {
			if (g.a[i][j] != INF) {
				cout << g.a[i][j] << " ";
			}
			else {
				cout << "INF" << " ";
			}
		}
		cout << endl;
	}
}

void floyd(Graph &g) {
	Graph p;
	p.n = g.n;
	for (int i = 0; i < g.n; i++) {
		for (int j = 0; j < g.n; j++) {
			p.a[i][j] = -1;
		}
	}

	for (int k = 0; k < g.n; k++) {
		for (int i = 0; i < g.n; i++) {
			for (int j = 0; j < g.n; j++) {
				if (g.a[i][j] > g.a[i][k] + g.a[k][j]) {
					g.a[i][j] = g.a[i][k] + g.a[k][j];
					p.a[i][j] = k;
				}
			}
		}
	}
	
	cout << "Shorted path: " << endl;
	printPathFloyd(g);
	cout << "Path: " << endl;
	printPathFloyd(p);
}


int main() {
	Graph g;
	g.n = 3;

	g.addEdge(0, 0, 0);
	g.addEdge(0, 1, 4);
	g.addEdge(0, 2, 5);
	g.addEdge(1, 0, 2);
	g.addEdge(1, 1, 0);
	g.addEdge(1, 2, INF);
	g.addEdge(2, 0, INF);
	g.addEdge(2, 1, -3);
	g.addEdge(2, 2, 0);

	floyd(g);
	return 0;
}