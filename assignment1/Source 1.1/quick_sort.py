import readfile
import time


def quick_sort(arr): 
    if len(arr) <= 1:
        return arr
    else:
        pivot = arr[0]
        less = []
        equal = []
        greater = []
        for item in arr:
        	if item < pivot:
        		less.append(item)
        	elif item == pivot:
        		equal.append(item)
        	else:
        		greater.append(item)
        return quick_sort(less) + equal + quick_sort(greater)

def qsort2(arr):
    if len(arr) <= 1:
        return arr
    else:
        return qsort2([item for item in arr[:] if item < arr[0]]) + [arr[0]] + qsort2(item for item in arr[:] if item > arr[0])

if __name__ == "__main__":
	arr = readfile.read_file('input.txt')
	
	start = time.time()
	arr = quick_sort(arr)
	end = time.time()

	print(end-start)
