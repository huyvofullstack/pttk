def read_file(filename):
	arr = []
	with open(filename) as f:
		line = f.read()
		arr = list(line)
		for i in range(len(arr)):
			arr[i] = int(arr[i])
	return arr
