import readfile
import time


def bubble_sort(arr):
	swap = True
	n = len(arr) - 1
	while n > 0 and swap:
		swap = False
		for i in range(n):
			if arr[i] > arr[i+1]:
				swap = True
				arr[i], arr[i+1] = arr[i+1], arr[i]
		n = n - 1
	return arr

if __name__ == "__main__":
	arr = readfile.read_file('input.txt')
	
	start = time.time()
	arr = bubble_sort(arr)
	end = time.time()

	print(end-start)
