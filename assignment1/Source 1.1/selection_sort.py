import readfile
import time


def selection_sort(arr):
	for i in range(len(arr)):
		min = i
		for j in range(i+1, len(arr)):
			if arr[j] < arr[min]:
				min = j
		arr[i], arr[min] = arr[min], arr[i]
	return arr

if __name__ == "__main__":
	arr = readfile.read_file('input.txt')
	
	start = time.time()
	arr = selection_sort(arr)
	end = time.time()

	print(end-start)
