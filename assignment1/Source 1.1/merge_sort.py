import readfile
import time


def merge_sort(arr):
	if len(arr) < 2:
		return arr
	result = []
	mid = len(arr) // 2
	left = arr[:mid]
	right = arr[mid:]
	left = merge_sort(left)
	right = merge_sort(right)
	while len(left) > 0 and len(right) > 0:
		if left[0] < right[0]:
			result.append(left[0])
			left.pop(0)
		else:
			result.append(right[0])
			right.pop(0)
	result = result + left + right
	return result

if __name__ == "__main__":
	arr = readfile.read_file('input.txt')
	
	start = time.time()
	arr = merge_sort(arr)
	end = time.time()

	print(end-start)
