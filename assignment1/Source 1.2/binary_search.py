def binary_search(list, key, last=None):
	if last is None:
		last = len(list)
	first = 0
	while first < last:
		mid = (first + last) // 2
		if list[mid] < key:
			first = mid + 1
		elif list[mid] > key:
			last = mid
		else:
			return mid
	return -1

if __name__ == '__main__':
	arr = []
	for i in range(100):
		arr.append(i)
	print(binary_search(arr, 80))
