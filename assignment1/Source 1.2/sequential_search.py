def sequential_search(arr, key):
	for index, item in enumerate(arr):
		if item == key:
			return index
	else:
		return -1

if __name__ == '__main__':
	arr = []
	for i in range(100):
		arr.append(i)
	print(sequential_search(arr, 80))
	